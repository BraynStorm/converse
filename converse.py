from typing import Union, List, Optional
from subprocess import run, STDOUT

from enum import IntEnum
from dataclasses import dataclass

try:
    import regex as regex
except ImportError:
    import re as regex

GIT_PATH: str = r"C:\Program Files\Git\cmd\git.exe"

CONVENTIONAL_REGEX_COMMIT_LINE = regex.compile(
    r"^(feat|fix|style|z|perf|refactor|ci|build|docs|script|res|internal|chore"
    r"|BREAKING)((?:\(\S+\))?)(!?): (.+)$",
    regex.IGNORECASE,
)
CONVENTIONAL_REGEX_TAG = regex.compile(
    r"^[vV](\d+).(\d+).(\d+).*",
    regex.IGNORECASE,
)


def git(args: Union[str, List[str]]):
    return run(
        [GIT_PATH, *args.split(" ")] if isinstance(args, str) else [GIT_PATH, *args],
        capture_output=True,
    )


def git_last_tag() -> str:
    return git("describe --abbrev=0").stdout.decode("utf-8")[:-1]


def git_log_since_tag(tag: str) -> List[str]:
    lines: List[str]

    return list(
        filter(
            None,
            git(f"log --format=%B {tag}..HEAD").stdout.decode("utf-8").split("\n"),
        )
    )


@dataclass(frozen=True)
class Version:
    major: int = 0
    minor: int = 0
    patch: int = 0

    def __str__(self) -> str:
        return f"v{self.major}.{self.minor}.{self.patch}"


class VersionChange(IntEnum):
    NO_CHANGE = -1
    INCREMENT_PATCH = 0
    INCREMENT_MINOR = 1
    INCREMENT_MAJOR = 2


@dataclass(frozen=True)
class CommitLine:
    fullString: str
    type: str
    scope: str
    breaking: bool
    description: str


def converse_parse_tag(tag: str) -> "Version":
    parts = CONVENTIONAL_REGEX_TAG.findall(tag)

    if parts:
        parts = parts[0]
        return Version(*list(map(int, parts)))
    else:
        return Version()


def converse_split_commit_line(line: str) -> "CommitLine":
    matches = CONVENTIONAL_REGEX_COMMIT_LINE.findall(line)[0]

    if len(matches) == 4:
        commit_type = matches[0]
        scope = matches[1][1:-1]
        breaking = bool(matches[2])
        description = matches[3]
        return CommitLine(line, commit_type, scope, breaking, description)
    else:
        return CommitLine(line, "", "", False, "")


def converse_history(lines: List[str]) -> List["CommitLine"]:
    return list(map(converse_split_commit_line, lines))


def converse_classify_commit_line(line: "CommitLine") -> VersionChange:
    if line.breaking:
        return VersionChange.INCREMENT_MAJOR
    elif line.type in {"feat", "perf"}:
        return VersionChange.INCREMENT_MINOR
    elif line.type:
        return VersionChange.INCREMENT_PATCH

    return VersionChange.NO_CHANGE


def converse_update_version(version: "Version", change: "VersionChange") -> "Version":
    if change == VersionChange.INCREMENT_MAJOR:
        return Version(version.major + 1, 0, 0)
    elif change == VersionChange.INCREMENT_MINOR:
        return Version(version.major, version.minor + 1, 0)
    elif change == VersionChange.INCREMENT_PATCH:
        return Version(version.major, version.minor, version.patch + 1)
    else:
        return Version(version.major, version.minor, version.patch)


def converse_get_tag_for_next_version() -> str:
    last_tag = git_last_tag()
    last_version = converse_parse_tag(last_tag)

    log = git_log_since_tag(last_tag)
    print("Last version:", last_tag)
    print("Commit lines after last version:", len(log))

    new_lines = converse_history(log)
    change = max(
        map(converse_classify_commit_line, new_lines), default=VersionChange.NO_CHANGE
    )
    print(change)
    new_version = converse_update_version(last_version, change)

    return str(new_version)


def converse_release_version(repository: Optional[str] = None):
    import os

    if repository is None:
        repository = os.getcwd()

    os.chdir(repository)

    tag_name = converse_get_tag_for_next_version()
    print("New version:", tag_name)
    git(f"tag -a -m {tag_name} {tag_name}")


def converse_main(*args):
    if args:
        converse_release_version(" ".join(args))
    else:
        converse_release_version()


if __name__ == "__main__":
    import sys

    converse_main(*sys.argv[1:])
