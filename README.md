# ConVerse - Conventional Versioning

Converse is a simple python script to automatically tag version releases, based on
ConventionalCommits and SemanticVersioning. The first version of any repository is
always `v0.0.0` and versions get incremented.
- any commit with an exclamation mark (`feat!`, `fix!`, etc.) increments the MAJOR
  version `v0.0.0` -> `v1.0.0`
- `feat` commits increment the MINOR version `v0.0.0` -> `v0.1.0`
- `fix` commits increment the PATCH version `v0.0.0` -> `v.0.0.1`

## Before use
You have to edit the script and make sure GIT_PATH is set to the correct executable.

## Usage
- `python converse.py` - modifies the repository in the current working directory
- `python converse.py <PATH_TO_REPOSITORY>` - modifies the repository at
  <PATH_TO_REPOSITORY>

## Requirements
- Python 3.7

## Related
- https://www.conventionalcommits.org/
- https://semver.org/
